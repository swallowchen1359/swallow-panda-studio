package firebase.auth;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.swallowpandastudio.swallowpandastudio.R;

public class LoginUtility {
    private static final String TAG = "LoginUtility";
    private static GoogleSignInOptions mGoogleSignInOptions;

    public static GoogleSignInClient googleLogin(Context context) {
        if (context == null) {
            Log.e(TAG, "googleLogin failed, context is null");
            return null;
        }

        String textString = context.getString(R.string.default_web_client_id);
        mGoogleSignInOptions = new GoogleSignInOptions
                                      .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                      .requestIdToken(textString)
                                      .requestEmail()
                                      .build();

        return GoogleSignIn.getClient(context, mGoogleSignInOptions);
    }

    public static void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        if ((acct == null)) {
            Log.d(TAG, "firebaseAuthWithGoogle failed, Google account is null");
        }

        Log.d(TAG, "firebaseAuthWithGoogle: " +acct.getId());

        FirebaseAuth auth = FirebaseAuth.getInstance();

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        auth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success");
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.getException());
                }
            }
        });
    }

    public static void googleLogout(Context context) {
        if (mGoogleSignInOptions == null) {
            Log.e(TAG, "googleLogout failed, no sign in before");
            return;
        }

        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(context, mGoogleSignInOptions);
        googleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.d(TAG, "googleLogout successfully");
            }
        });
    }

    public static void firebaseAuthSignOut() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signOut();
    }
}
