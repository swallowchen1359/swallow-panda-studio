package com.swallowpandastudio.swallowpandastudio;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import firebase.database.VocabulazyDatabase;

public class FirebaseDataBrowserActivity extends AppCompatActivity {

    private static final String TAG = "FirebaseDataBrowser";
    private FirebaseDatabase mFirebaseDatabase;
    private ArrayList mDatabaseDataContent_book;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_data_browser);

        mFirebaseDatabase = FirebaseDatabase.getInstance();

        final TextView textView = findViewById(R.id.firebase_database_content_text_view);

        Button attachButton = findViewById(R.id.attach_button);
        attachButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("");
            }
        });

        Button deattachButton = findViewById(R.id.deattach_button);
        deattachButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("Get Firebase Database Data");
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        DatabaseReference ref = mFirebaseDatabase.getReference();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                VocabulazyDatabase vocabulazyDatabase = dataSnapshot.getValue(VocabulazyDatabase.class);
                Log.d(TAG, "DatabaseReference.onDataChange" + vocabulazyDatabase.book);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
