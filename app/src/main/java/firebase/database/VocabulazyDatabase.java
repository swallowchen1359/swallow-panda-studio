package firebase.database;

import java.util.HashMap;
import java.util.List;

public class VocabulazyDatabase extends HashMap<String, Object> {
    public List<HashMap<String, Object>> book;
    public List<HashMap<String, Object>> category;
    public HashMap<String, Object> lesson;
    public HashMap<String, Object> mentor;
    public HashMap<String, Object> user;
    public HashMap<String, Object> vocabulary;

    public VocabulazyDatabase() {

    }

    public VocabulazyDatabase(List<HashMap<String, Object>> book, List<HashMap<String, Object>> category, HashMap<String, Object> lesson, HashMap<String, Object> mentor, HashMap<String, Object> user, HashMap<String, Object> vocabulary) {
        this.book = book;
        this.category = category;
        this.lesson = lesson;
        this.mentor = mentor;
        this.user = user;
        this.vocabulary = vocabulary;
    }
}
